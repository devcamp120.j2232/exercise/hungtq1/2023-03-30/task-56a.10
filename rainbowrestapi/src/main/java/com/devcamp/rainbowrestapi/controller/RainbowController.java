package com.devcamp.rainbowrestapi.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class RainbowController {
    @GetMapping("/rainbow")
    public ArrayList<String> getRainbowColor(){
        ArrayList<String> colors = new ArrayList<>();

        colors.add("red");
        colors.add("blue");
        colors.add("yellow");
        colors.add("white");
        colors.add("green");
        colors.add("orange");


        return colors;
    }




}
